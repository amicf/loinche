#!/usr/bin/env sh

set -euf

error() {
    >&2 echo "$1"
    exit 1
}

if [ -z "${1+x}" ]; then
   error "Please provide the production directory in parameter."
fi

if [ "$(id -u)" = "0" ]; then
   error "Not running as root. Please run me as your webserver's user for instance, using something like sudo -H -u www-data $0 $1"
fi

PROD_DIR="$1"

if ! [ -d "$PROD_DIR" ]; then
   error "The production directory $PROD_DIR does not exist. I give up."
fi

if [ -d "$PROD_DIR/.git" ]; then
   error "The production folder should not be a git repository."
fi

WORKING_DIRECTORY="$(dirname "$(dirname "$0")")"

if ! [ -d "$WORKING_DIRECTORY/.git" ]; then
    error "Not a git repository. Cannot update."
fi

if [ -f "$WORKING_DIRECTORY/games.backup.json" ]; then
    error "Your working copy has a 'games.backup.json' file. I give up."
fi

if ! [ -z "$(git status --porcelain)" ]; then
    error "Your git repository is not clean. I am not doing anything."
fi

# Thx https://stackoverflow.com/questions/17414104/git-checkout-latest-tag

echo "Fetching updates from git"
git fetch
new_version="$(git tag --list '[prod]*' --sort=v:refname | tail -1)"

echo "Checking out ${new_version}..."
git checkout "$new_version"

echo "Setting up production mode..."
"$(dirname $0)"/setup_prod.sh

BACKUP_DIR="$WORKING_DIRECTORY/../trivabble-prod.bak"

echo "Backing up production to $BACKUP_DIR ..."
rsync -avp --delete "$PROD_DIR/" "$BACKUP_DIR/"

echo "Sending to production..."
rsync -avp --delete --exclude 'games.backup.json' --exclude 'config.js' --exclude '/.git' "$WORKING_DIRECTORY/" "$PROD_DIR/"

echo echo "Resetting the directory..."
git reset --hard "$new_version"
