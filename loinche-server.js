/* eslint-disable no-process-env */
/**
 * Copyright (C) 2020-2020 Amic Frouvelle <loinche-amic@sandrine.da-et-amic.fr>
 * Copyright (C) 2016-2020 Raphaël Jakse <raphael.trivabble@jakse.fr>
 *
 * @licstart
 * This file is part of Loinche (based on Trivabble version 202004281800)
 *
 * Loinche is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License (GNU AGPL)
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Loinche is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Loinche.  If not, see <http://www.gnu.org/licenses/>.
 * @licend
 *
 * @source: https://trivabble.1s.fr/
 * @source: https://gitlab.com/raphj/trivabble/
 */

/*eslint strict: [2, "global"]*/

"use strict";

const port = parseInt(process.env.LOINCHE_PORT || "3000");
const SAVE_TIMEOUT = 5000;
const KEEP_ALIVE = 30000;
const GAMES_BACKUP = process.env.LOINCHE_GAMES_BACKUP || "games.backup.json";

const VERSION = 202005240300;

function envTrue(name) {
    return (process.env[name] || "").toLowerCase() === "true";
}

const DEV_ENABLE_SERVING_FILES = envTrue("DEV_ENABLE_SERVING_FILES");
const DEBUG_LOG = DEV_ENABLE_SERVING_FILES || envTrue("DEBUG_LOG");

if (DEV_ENABLE_SERVING_FILES) {
    console.log("DEV_ENABLE_SERVING_FILES: Serving files in the current directory. Please never do this on a production server, this is for development purposes only.");
}

const debuglog = DEBUG_LOG ? console.log.bind(console) : () => null;

const http = require("http");
const fs   = require("fs");
const crypto = require("crypto");

const REQUEST_TYPE_LONG_POLLING = 1;
const REQUEST_TYPE_SSE          = 2;
const REQUEST_TYPE_WEBSOCKET    = 3;

const fr32Deck = [
    "club_10", "club_1", "club_7", "club_8", "club_9", "club_jack", "club_king", "club_queen",
    "diamond_10", "diamond_1", "diamond_7", "diamond_8", "diamond_9", "diamond_jack", "diamond_king", "diamond_queen",
    "heart_10", "heart_1", "heart_7", "heart_8", "heart_9", "heart_jack", "heart_king", "heart_queen",
    "spade_10", "spade_1", "spade_7", "spade_8", "spade_9", "spade_jack", "spade_king", "spade_queen"
];


const games = {};

let saveTimeout = null;
let running = true;
const server = http.createServer(handleRequest);
server.setTimeout(0); // The default in node 13 is 0. Earlier versions have 120.

function saveGames(callback) {
    fs.writeFile(GAMES_BACKUP, JSON.stringify(games), function (err) {
        if (err) {
            console.error("ERROR: Cannot save games!");
        }

        if (typeof callback === "function") {
            return callback(err);
        }
    });

    saveTimeout = null;
}


function shuffleInPlace(a) {
    // https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array

    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
}

function Game() {
    this.init();
    this.listeningPlayers = [];
    this.pendingEvents = [];
}

function writeMessage(responseAndType, data, terminate) {
    if (!responseAndType[0]) {
        return;
    }

    if (responseAndType[1] === REQUEST_TYPE_WEBSOCKET) {
        webSocketWrite(responseAndType[0], data);
    } else {
        responseAndType[0][terminate ? "end" : "write"](
            responseAndType[1] === REQUEST_TYPE_SSE
                ? "data:" + data + "\n\n"
                : data.length + data
        );
    }

    if (terminate) {
        stopKeepAlive(responseAndType);
        if (responseAndType[1] === REQUEST_TYPE_WEBSOCKET && responseAndType[0] && !responseAndType[0].isDestroyed) {
            responseAndType[0].end(webSocketCloseBuffer);
            responseAndType[0] = null;
        }
    } else {
        keepAlive(responseAndType);
    }
}

function stop() {
    console.log("Saving games a first time");
    if (saveTimeout) {
        clearTimeout(saveTimeout);
        saveTimeout = null;
    }

    saveGames(function () {
        running = false;
        console.log("Closing connections and saving the game state...");

        let listeningPlayerCount = 0;
        let gamesCount = 0;

        for (const gameID of Object.keys(games)) {
            const game = games[gameID];
            for (const listeningPlayer of game.listeningPlayers) {
                writeMessage(listeningPlayer, '{"stopping": 2000}', true);
            }

            if (game.listeningPlayers.length) {
                listeningPlayerCount += game.listeningPlayers.length;
                gamesCount++;
            }
        }

        console.log(
            "Stopped", gamesCount, "game" + (gamesCount === 1 ? "" : "s"),
            "and", listeningPlayerCount, "player connection" + (listeningPlayerCount === 1 ? "" : "s") +
            "."
        );

        server.close(saveGames);
    });
}

const webSocketPingBuffer = Buffer.from([
    0b10001001, 0b00000000
]);

const webSocketCloseBuffer = Buffer.from([
    0b10001000, 0b00000000
]);


function stopKeepAlive(responseAndType) {
    if (responseAndType.keepAliveTimeout) {
        clearTimeout(responseAndType.keepAliveTimeout);
        responseAndType.keepAliveTimeout = 0;
    }
}

function keepAlive(responseAndType) {
    stopKeepAlive(responseAndType);
    responseAndType.keepAliveTimeout = setTimeout(function keepAliveTimeout() {
        responseAndType[0].write(
            responseAndType[1] === REQUEST_TYPE_WEBSOCKET
                ? webSocketPingBuffer
                : (
                    responseAndType[1] === REQUEST_TYPE_SSE
                        ? ":\n\n"
                        : "2[]"
                )
        );
    }, KEEP_ALIVE);
}


Game.prototype.init = function () {
    this.baize = [];       // Of the form [{cardname:"club_1",position:{top:"3%",left:"4%"},seat:0}, … ] : cards on the baize
    this.evenTricks = [];   // Of the form [{cardname:"club_1",position:{…},seat:0}, … ] : tricks of players 0 and 2 (with their original positions on the baize)
    this.oddTricks = [];   // Of the form [{cardname:"club_1",position:{…},seat:0}, … ] : tricks of players 1 and 3 (with their original positions on the baize)
    this.deck = fr32Deck.slice();
    this.hands = {};
    this.seats = {};
    this.names = ["","","",""];
    this.teamScores = [0, 0];
    this.lastUpdated = new Date();

    shuffleInPlace(this.deck);
};

Game.prototype.toJSON = function () {
    return {
	baize:            this.baize,
	evenTricks:       this.evenTricks,
	oddTricks:        this.oddTricks,
	deck:             this.deck,
	hands:            this.hands,
	seats:            this.seats,
	names:            this.names,
	teamScores:       this.teamScores,
        lastUpdated:      this.lastUpdated.toISOString()
    };
};

Game.fromJSON = function (obj) {
    const game = new Game();
    game.baize            = obj.baize         || [];
    game.evenTricks       = obj.evenTricks    || [];
    game.oddTricks        = obj.oddTricks     || [];
    game.deck             = obj.deck          || fr32Deck.slice();
    game.hands            = obj.hands         || {};
    game.seats            = obj.seats         || {};
    game.names            = obj.names         || ["","","",""];
    game.teamScores       = obj.teamScores    || [0, 0];
    game.lastUpdated      = obj.lastUpdated ? new Date(obj.lastUpdated) : new Date();
    return game;
};

Game.prototype.getPlayerHand = function (player) {
    const playerID = "#" + player;
    return (this.hands[playerID] || (this.hands[playerID] = []));
};

Game.prototype.getPlayerSeat = function (player, wantedSeat) {
    const playerID = "#" + player;
    if (typeof this.seats[playerID] == "number") {
	return this.seats[playerID];
    } else if ((wantedSeat >= 0) && !this.names[wantedSeat]) {
	this.names[wantedSeat] = player;
	return (this.seats[playerID] = wantedSeat);
    } else {
	for (let i = 0; i < 4; i++) {
	    if (!this.names[i]) {
		this.names[i] = player;
		return (this.seats[playerID] = i);
	    }
	}
	return (this.seats[playerID] = -1); // no more seat : block ? Or access as spectator
    }
};

Game.prototype.setTeamScore = function (score, parity) {
    this.teamScores[parity] = score;
    this.pendingEvents.push({
        teamScore: score,
	parity: parity
    });
};

Game.prototype.setPlayerSeat = function (player, seat) {
    const playerID = "#" + player;

    this.seats[playerID] = seat;
    this.names[seat] = player;

    this.pendingEvents.push({
        players: [{
            player: player,
	    seat: seat,
	    handCount: countCards(this.getPlayerHand(player))
        }]
    });
};

Game.prototype.exchangeSeats = function (from, to) {
    const playerFrom = this.names[from];
    const playerTo = this.names[to];

    const playerIDFrom = "#" + playerFrom;
    const playerIDTo = "#" + playerTo;

    this.seats[playerIDFrom] = to;
    this.seats[playerIDTo] = from;
    this.names[from] = playerTo;
    this.names[to] = playerFrom;

    let players = []
    for (let seat = 0; seat < 4; seat++) {
	let player = this.names[seat];
	players.push({
            player: player,
	    seat: seat,
	    handCount: countCards(this.getPlayerHand(player))
        })
    }
    this.pendingEvents.push({
        players: players
    });
};

Game.prototype.playerJoined = function (playerName, wantedSeat) {
    if (playerName) {
        this.getPlayerHand(playerName); // Create the player's hand
        this.getPlayerSeat(playerName, wantedSeat); // Give the player's seat
    }

    const players = [];

    for (let player in this.hands) {
        if (Object.prototype.hasOwnProperty.call(this.hands, player)) {
            player = player.slice(1); // '#'
            players.push({
                player: player,
		seat: this.getPlayerSeat(player),
                handCount: countCards(this.getPlayerHand(player))
            });
        }
    }

    this.pendingEvents.push({players: players});
    this.pendingEvents.push({teamScore: this.teamScores[0],parity: 0});
    this.pendingEvents.push({teamScore: this.teamScores[1],parity: 1});
};

Game.prototype.addListeningPlayer = function (playerName, responseAndType) {
    const that = this;

    that.listeningPlayers.push(responseAndType);
    keepAlive(responseAndType);

    let closed = false;

    function close () {
        if (closed) {
            return;
        }

        closed = true;
        stopKeepAlive(responseAndType);
        responseAndType[0] = null;

        const index = that.listeningPlayers.indexOf(responseAndType);
        if (index !== -1) {
            that.listeningPlayers[index] = that.listeningPlayers[that.listeningPlayers.length - 1];
            that.listeningPlayers.pop();
        }
    }

    responseAndType[0].on("error", close);
    responseAndType[0].on("close", close);
    responseAndType[0].on("finish", close);
    responseAndType[0].on("prefinish", close);

    this.playerJoined(playerName);
    this.commit();
};

Game.prototype.commit = function () {
    const msg = JSON.stringify(this.pendingEvents);
    this.pendingEvents = [];

    for (let i = 0; i < this.listeningPlayers.length; i++) {
        while (i < this.listeningPlayers.length && !this.listeningPlayers[i]) {
            this.listeningPlayers[i] = this.listeningPlayers[this.listeningPlayers.length - 1];
            this.listeningPlayers.pop();
        }

        if (this.listeningPlayers[i]) {
            writeMessage(this.listeningPlayers[i], msg);
        }
    }

    if (saveTimeout === null) {
        saveTimeout = setTimeout(saveGames, SAVE_TIMEOUT);
    }
};

Game.prototype.deckTakeCard = function (player) {
    if (this.deck.length) {
        const card = this.deck.pop();
        this.pendingEvents.push({
            player: player,
            action: "takeDeck",
            remainingCards: this.deck.length
        });

        return card;
    }

    return "";
};

Game.prototype.baizeGetCard = function (index) {
    return this.baize[index].cardname;
};

Game.prototype.deckPutCard = function (card, player) {
    if (card) {
        this.deck.push(card);
        // shuffleInPlace(this.deck);

        this.pendingEvents.push({
            player: player,
            action: "putDeck",
            remainingCards: this.deck.length
        });
    }
};

Game.prototype.baizePushCard = function (position, card, player, seat) {
    if (card) {
	this.baize.push({
	    seat: seat,
	    position: position,
	    cardname: card
	});
        this.pendingEvents.push({
            player: player,
	    seat: seat,
            action: "baizePushCard",
	    position: position,
	    cardname: card	    
        });
    }
}

Game.prototype.cutDeck = function (indexCut) {
    const startDeck = this.deck.splice(0,indexCut);
    this.deck = this.deck.concat(startDeck);
}

Game.prototype.makeDeck = function (parityAbove) {
    function copyOnDeck(theDeck, tricks) {
	for (let i = 0; i < tricks.length; i++) {
	    theDeck.push(tricks[i].cardname);
	}
    }
    if (parityAbove == 0) {
	copyOnDeck(this.deck, this.oddTricks);
	copyOnDeck(this.deck, this.evenTricks);
    } else {
	copyOnDeck(this.deck, this.oddTricks);
	copyOnDeck(this.deck, this.evenTricks);
    }
    this.oddTricks = [];
    this.evenTricks = [];
    
    this.pendingEvents.push({
	remainingCards: this.deck.length,
	oddTricksLength: 0,
	evenTricksLength: 0
    });
}

Game.prototype.showTrick = function (parity) {
    for (let i = 0; i < 4; i++) {
	let trickCard = null;
	if (parity == 0) {
	    trickCard = this.evenTricks.pop();
	} else {
	    trickCard = this.oddTricks.pop();
	}
	if (trickCard) {
	    this.baize.push(trickCard);
	}
    }
    if (parity ==0) {
	this.pendingEvents.push({
	    baize: this.baize,
	    evenTricksLength: this.evenTricks.length
	});
    } else {
	this.pendingEvents.push({
	    baize: this.baize,
	    oddTricksLength: this.oddTricks.length
	});
    }
}

Game.prototype.takeTrick = function (parity) {
    while (this.baize.length > 0) {
	let baizeCard = this.baize.pop()
	if (baizeCard) {
	    if (parity == 0) {
		this.evenTricks.push(baizeCard);
	    } else {
		this.oddTricks.push(baizeCard);
	    }
	}
    }
    if (parity == 0) {
	this.pendingEvents.push({
	    baize: [],
	    evenTricksLength: this.evenTricks.length 
	});
    } else {
	this.pendingEvents.push({
	    baize: [],
	    oddTricksLength: this.oddTricks.length 
	});
    }
}

Game.prototype.baizeRemoveCard = function (index, player) {
    this.baize.splice(index,1);	
    this.pendingEvents.push({
        action: "baizeRemoveCard",
	player: player,
	index: index
    });
    
}

Game.prototype.deal = function (seat, nCards) {
    let player = this.names[seat];
    if (player) {
	let hand = this.getPlayerHand(player);
	let i = 0;
	let card = null;
	for (let n = 0; n < nCards; n++) {
	    while (hand[i]) {
		i++;
	    }
	    if (i >=8) { break; }
	    card = this.deckTakeCard(player);
	    hand[i] = card;
	}
	this.pendingEvents.push({
	    players: [{
		player: player,
		seat: seat,
		handCount: countCards(hand)
            }],
	    player: player,
	    hand: hand,
	    remainingCards: this.deck.length
	});
    }
}


Game.prototype.reset = function (player) {
    this.init();
    this.pendingEvents.push({
        player: player,
        action: "reset",
	baize: this.baize,
	evenTricksLength: this.evenTricks.length,
	oddTricksLength: this.oddTricks.length,
	remainingCards: this.deck.length,
	hand: []
    });

    this.playerJoined();
};

function newGameId() {
    let number;

    let k = 10000;
    let retries = 0;

    do {
        number = Math.floor(Math.random() * k).toString();

        if (retries > 10) {
            retries = 0;
            k *= 10;
        } else {
            retries++;
        }
    } while (games[number]);

    return number.toString();
}


function countCards(hand) {
    let count = 0;

    for (let i = 0; i < hand.length; i++) {
        if (hand[i]) {
            count++;
        }
    }

    return count;
}


function joinGame(gameNumber) {
    if (!gameNumber) {
        gameNumber = newGameId();
    }

    const game = games[gameNumber] || (games[gameNumber] = new Game());

    return {gameNumber, game};
}

function reply(message, response, cmdNumber, r) {
    response.write(
        JSON.stringify({...r, id: message.id || message.cmds[cmdNumber].id})
    );
}

function handleCommand(cmdNumber, message, response) {
    const {gameNumber, game} = joinGame(message.gameNumber);

    game.lastUpdated = new Date();

    const playerName = message.playerName;

    let hand = null;
    const cmd = message.cmds[cmdNumber];

    switch (cmd.cmd) {
        case "joinGame": {
            reply(message, response, cmdNumber, {
                error: 0,
                gameNumber: gameNumber,
                playerName: playerName,
 		playerSeat: game.getPlayerSeat(playerName),
 		hand: game.getPlayerHand(playerName),
		baize: game.baize,
		evenTricksLength: game.evenTricks.length,
		oddTricksLength: game.oddTricks.length,
		remainingCards: game.deck.length,
		version: VERSION
            });
            break;
        }

        case "hello": {
            game.playerJoined(playerName, cmd.wantedSeat);
            reply(message, response, cmdNumber, {error: 0, version: VERSION});
            break;
        }

	case "teamScore": {
            game.setTeamScore(cmd.score, cmd.parity);
            reply(message, response, cmdNumber, {error: 0});
            break;
        }

        case "makeDeck": {
	    game.makeDeck(cmd.parityAbove);
            reply(message, response, cmdNumber, {error: 0});
	    break;
	}

        case "cutDeck": {
	    game.cutDeck(cmd.indexCut);
            reply(message, response, cmdNumber, {error: 0});
	    break;
	}

        case "showTrick": {
	    game.showTrick(cmd.parity);
            reply(message, response, cmdNumber, {error: 0});
	    break;
	}
    
        case "takeTrick": {
	    game.takeTrick(cmd.parity);
            reply(message, response, cmdNumber, {error: 0});
	    break;
        }
        // Replace here by a lot of commands instead of moveCard ?
	// Like removeCard, addCard ? (so they are handled at dragBegin and dragEnd)

	case "deal": {
	    game.deal(cmd.seatTo, cmd.nCards);
            reply(message, response, cmdNumber, {error: 0});
	    break;
	}

	case "clearHand": {
	    hand = game.getPlayerHand(playerName);
	    const seat = game.getPlayerSeat(playerName)
	    for (let i = 0; i < 8; i++) {
		if (hand[i]) {
		    // No player here so it is seen by the playing one (not a dragdrop)
		    game.baizePushCard({top: 0.8, left: (0.22 + 0.08 * i) }, hand[i], "", seat);
		    hand[i] = "";
		}
	    }
	    game.pendingEvents.push({
                players: [{
                    player: playerName,
		    seat: seat,
                    handCount: 0
                }],
		player: playerName,
		hand: []
            });	

            reply(message, response, cmdNumber, {error: 0});

            break;
	}

        case "moveCard": {
            let card = "";

            // This case can fail in various ways. Instead altering the state
            // of the game immediately, we store the operations to run them
            // at the very end, if nothing failed.
            const operations = [];

            switch (cmd.from) {
                case "hand":
                    hand = game.getPlayerHand(playerName);

                    if (cmd.indexFrom > 7 || cmd.indexFrom < 0) {
                        reply(message, response, cmdNumber, {error: 1, reason: "Wrong indexFrom"});
                        return false;
                    }

                    card = hand[cmd.indexFrom];

                    if (!card) {
                        reply(message, response, cmdNumber, {error: 1, reason: "Moving from an empty location"});
                        return false;
                    }

                    operations.push(() => {
                        hand[cmd.indexFrom] = "";
                    });
                    break;

                case "baize":
                    if (cmd.indexFrom < 0 || cmd.indexFrom >= game.baize.length) {
                        reply(message, response, cmdNumber, {error: 1, reason: "Wrong indexFrom"});
                        return false;
                    }

                    card = game.baizeGetCard(cmd.indexFrom);
                    operations.push(game.baizeRemoveCard.bind(game, cmd.indexFrom, playerName));
                    break;

                case "deck":
                    if (!game.deck.length) {
                        reply(message, response, cmdNumber, {error: 1, reason: "Empty deck"});
                        return false;
                    }

                    operations.push(() => {
                        card = game.deckTakeCard(playerName);
                    });
                    break;

                default:
                    reply(message, response, cmdNumber, {error: 1, reason: "Moving card from an unknown place"});
                    return false;
            }
	   
            switch (cmd.to) {
                case "hand":
                    if (cmd.indexTo < 0 || cmd.indexTo > 7) {
                        reply(message, response, cmdNumber, {error: 1, reason: "Wrong indexTo"});
                        return false;
                    }

                    hand = hand || game.getPlayerHand(playerName);

                    if (hand[cmd.indexTo]) {
                        reply(message, response, cmdNumber, {error: 1, reason: "Moving a card to a non-empty location"});
                        return false;
                    }

                    operations.push(() => {
                        hand[cmd.indexTo] = card;
                    });
                    break;
		
                case "baize":
                    operations.push(game.baizePushCard.bind(game, cmd.positionTo, card, playerName, game.getPlayerSeat(playerName)));
                    break;

                case "deck":
                    operations.push(game.deckPutCard.bind(game, card, playerName));
                    break;

                default:
                    response.write("{\"error\":1, \"reason\":\"Moving card to an unknown place\"}");
                    return false;
            }

            for (const operation of operations) {
                operation();
            }

            reply(message, response, cmdNumber, {
                error: 0,
                hand: (
                    (cmd.from === "deck" && cmd.to === "hand")
                        ? hand
                        : undefined // eslint-disable-line no-undefined
                ),
                remainingCards: game.deck.length
            });

            if (hand) {
                game.pendingEvents.push({
                    players: [{
                        player: playerName,
			seat: game.getPlayerSeat(playerName),
                        handCount: countCards(hand)
                    }]
                });
            }
            break;
        }

        case "setHand": {
            if (cmd.hand.length > 8) {
                reply(message, response, cmdNumber, {
                    error: 1,
                    hand: hand,
                    reason: "the new hand is not at the right size"
                });
                return false;
            }

            hand = game.getPlayerHand(playerName);

            const oldHandSorted = hand.filter((l) => l);
            oldHandSorted.sort();

            const newHandSorted = cmd.hand.filter((l) => l);
            newHandSorted.sort();

            for (let i = 0; i < 7; i++) {  // I don’t understand that…
                if ((oldHandSorted[i] !== newHandSorted[i]) && (oldHandSorted[i] || newHandSorted[i])) {
                    reply(message, response, cmdNumber, {
                        error: 1,
                        hand: hand,
                        reason: "the new hand doesn't contain the same number of cards"
                    });
                    return false;
                }
            }

            for (let i = 0; i < 8; i++) {
                hand[i] = cmd.hand[i];
            }

            reply(message, response, cmdNumber, {error: 0});

            break;
        }

        case "exchangeSeats": {
	    game.exchangeSeats(cmd.from, cmd.to);	    
            reply(message, response, cmdNumber, {error: 0});
            break;
        }

        case "resetGame": {
            game.reset();
            reply(message, response, cmdNumber, {error: 0});
            break;
        }

        case "msg": {
            game.pendingEvents.push({
                msg: {
                    sender: playerName,
                    content: cmd.msg,
                    specialMsg: cmd.specialMsg
                }
            });

            reply(message, response, cmdNumber, {error: 0});
            break;
        }

        default: {
            reply(message, response, cmdNumber, {error: 1, reason: "Unknown command"});
            return false;
        }
    }

    return true;
}

function handleCommands(message, responseAndType) {
    if (!message.cmds || !message.cmds.length) {
        const {gameNumber, game} = joinGame(message.gameNumber);

        writeMessage(responseAndType,
            JSON.stringify({
                playerName:       message.playerName,
                playerSeat:       game.getPlayerSeat(message.playerName),
                gameNumber:       gameNumber,
                hand:             game.getPlayerHand(message.playerName),
		baize:            game.baize,
		evenTricksLength: game.evenTricks.length,
		oddTricksLength:  game.oddTricks.length,
                remainingCards:   game.deck.length,
                version:          VERSION
            })
        );

        game.addListeningPlayer(message.playerName, responseAndType);
        return;
    }

    let wsMessage = "";

    const response = (
        responseAndType[1] === REQUEST_TYPE_WEBSOCKET
            ? {
                write: function (s) {
                    wsMessage += s;
                },

                end: function (s) {
                    if (s) {
                        wsMessage += s;
                    }
                    webSocketWrite(responseAndType[0], wsMessage);
                }
            }
            : responseAndType[0]
    );

    response.write("[");

    for (let i = 0; i < message.cmds.length; i++) {
        if (i) {
            response.write(",");
        }

        if (!handleCommand(i, message, response)) {
            break;
        }
    }

    response.end("]");

    if (games[message.gameNumber]) {
        games[message.gameNumber].commit();
    }
}

// Thx https://medium.com/hackernoon/implementing-a-websocket-server-with-node-js-d9b78ec5ffa8

function webSocketWrite(socket, data) {
    /* eslint-disable capitalized-comments */

    // Copy the data into a buffer
    data = Buffer.from(data);
    const byteLength = data.length;

    // Note: we're not supporting > 65535 byte payloads at this stage
    const lengthByteCount = byteLength < 126 ? 0 : 2;
    const payloadLength = lengthByteCount === 0 ? byteLength : 126;
    const buffer = Buffer.alloc(2 + lengthByteCount + byteLength);

    // Write out the first byte, using opcode `1` to indicate that the message
    // payload contains text data
    buffer.writeUInt8(0b10000001, 0);
    buffer.writeUInt8(payloadLength, 1);

    // Write the length of the payload to the second byte

    if (lengthByteCount === 2) {
        buffer.writeUInt16BE(byteLength, 2);
    }

    // Write the data to the data buffer
    data.copy(buffer, 2 + lengthByteCount);
    socket.write(buffer);
}

function webSocketGetMessage(buffer) {
    /* eslint-disable no-bitwise, capitalized-comments */

    let dataAfter = "";
    let finalOffset = -1;

    if (buffer.length < 2) {
        return ["", 0];
    }

    const firstByte = buffer.readUInt8(0);


    const isFinalFrame = Boolean((firstByte >>> 7) & 0x1);
    // const [reserved1, reserved2, reserved3] = [
    //     Boolean((firstByte >>> 6) & 0x1),
    //     Boolean((firstByte >>> 5) & 0x1),
    //     Boolean((firstByte >>> 4) & 0x1)
    // ];

    const opCode = firstByte & 0xF;

    // We can return null to signify that this is a connection termination frame
    if (opCode === 0x8) {
        return null;
    }

    const secondByte = buffer.readUInt8(1);
    const isMasked = (secondByte >>> 7) & 0x1;

    // Keep track of our current position as we advance through the buffer
    let currentOffset = 2;
    let payloadLength = secondByte & 0x7F;
    if (payloadLength > 125) {
        if (payloadLength === 126) {
            payloadLength = buffer.readUInt16BE(currentOffset);
            currentOffset += 2;
        } else {
            // 127
            // If this has a value, the frame size is ridiculously huge!
            // const leftPart = buffer.readUInt32BE(currentOffset);
            // const rightPart = buffer.readUInt32BE(currentOffset += 4);
            // Honestly, if the frame length requires 64 bits, you're probably doing it wrong.
            // In Node.js you'll require the BigInt type, or a special library to handle this.
            throw new Error("Large websocket payloads not currently implemented");
        }
    }

    if (payloadLength + currentOffset > buffer.length) {
        return ["", 0];
    }

    if (!isFinalFrame) {
        const message = webSocketGetMessage(buffer.slice(payloadLength + currentOffset));
        if (message) {
            if (message[1]) {
                dataAfter = message[0];
                finalOffset = message[1];
            } else {
                return ["", 0];
            }
        } else if (message === null) {
            // ??!?
            return null;
        }
    }

    let maskingKey;
    if (isMasked) {
        maskingKey = buffer.readUInt32BE(currentOffset);
        currentOffset += 4;
    }

    // Allocate somewhere to store the final message data
    const data = Buffer.alloc(payloadLength);

    // Only unmask the data if the masking bit was set to 1
    if (isMasked) {
        // Loop through the source buffer one byte at a time, keeping track of which
        // byte in the masking key to use in the next XOR calculation
        for (let i = 0, j = 0; i < payloadLength; ++i, j = i % 4) {
            // Extract the correct byte mask from the masking key
            const shift = (j === 3) ? 0 : (3 - j) << 3;
            const mask = (shift === 0 ? maskingKey : (maskingKey >>> shift)) & 0xFF;

            // Read a byte from the source buffer
            const source = buffer.readUInt8(currentOffset++);

            // XOR the source byte and write the result to the data buffer
            data.writeUInt8(mask ^ source, i);
        }
    } else {
        // Not masked - we can just read the data as-is
        buffer.copy(data, 0, currentOffset);
    }

    return [
        opCode === 0x1
            ? (data.toString("utf8") + dataAfter)
            : "",
        finalOffset === -1
            ? currentOffset
            : finalOffset
    ];
}

function handleRequest(request, response) {
    let post = "";
    const responseAndType = [response, REQUEST_TYPE_LONG_POLLING];
    let upgradedToWebSocket = false;

    response.on("error", function connectionError(e) {
        console.error("An error occurred while trying to write on a socket", e);
        stopKeepAlive(responseAndType);
    });

    // Thx http://stackoverflow.com/questions/4295782/how-do-you-extract-post-data-in-node-js
    request.on("data", function (data) {
        if (upgradedToWebSocket) {
            return;
        }

        post += data;

        // Too much POST data, kill the connection!
        if (post.length > 1e6) {
            request.connection.destroy();
        }
    });

    request.on("upgrade", function () {
        upgradedToWebSocket = true;
    });

    request.on("error", function (error) {
        console.error("Error while handling this request", request, error);
    });

    request.on("end", function () {
        if (upgradedToWebSocket) {
            return;
        }

        if (!running) {
            response.statusCode = 503;
            response.statusMessage = "Server is stopping";
            response.end("Server is stopping");
            return;
        }

        if (DEV_ENABLE_SERVING_FILES && !request.url.startsWith("/:")) {
            if (request.url === "/") {
                request.url = "/index.html";
            }

            debuglog("Serving " + request.url);

            fs.exists("." + request.url, function (exists) {
                if (exists) {
                    fs.readFile("." + request.url, function(err, contents) {
                        if (err) {
                            response.statusCode = 500;
                            response.setHeader("Content-Type", "text/plain; charset=utf-8");
                            response.end(err);
                        }

                        let mime = "application/xhtml+xml; charset=utf-8";
                        const mimes = {
                            ".mp3": "audio/mpeg",
                            ".ogg": "audio/ogg",
                            ".js": "application/javascript; charset=utf-8",
                            ".css": "text/css; charset=utf-8",
                            ".svg": "image/svg+xml"
                        };

                        for (const i in mimes) {
                            if (Object.prototype.hasOwnProperty.call(mimes, i) && request.url.endsWith(i)) {
                                mime = mimes[i];
                                if (i === ".js") {
                                    contents = contents.toString().replace(
                                        /(?<before>^|\s|\()(?:const|let)(?<after>\s)/gu,
                                        "$1var$2"
                                    );
                                }
                                break;
                            }
                        }

                        response.setHeader("Content-Type", mime);
                        response.end(contents);
                    });
                } else {
                    response.statusCode = 404;
                    response.end("404");
                }
            });

            return;
        }

        const sseMatch = request.url.match(/\/(?::[^/]+\/)?sse\/(?<data>[\s\S]+)$/u);

        if (sseMatch) {
            response.setHeader("Content-Type", "text/event-stream");
            post = decodeURIComponent(sseMatch.groups.data);
            responseAndType[1] = REQUEST_TYPE_SSE;
        } else {
            response.setHeader("Content-Type",   "text/plain");
            response.setHeader("Transfer-Encoding", "chunked");
        }

        debuglog("REQ:", request.url, post, responseAndType[1]);

        try {
            post = post && JSON.parse(post);
        } catch (e) {
            response.statusCode = 400;
            response.setHeader("application/json");
            response.end('{"error":1, "reason": "Unable to parse your JSON data"}');
            return;
        }

        handleCommands(post, responseAndType);
    });
}

fs.readFile(GAMES_BACKUP, function (err, data) {
    try {
        if (err) {
            console.error("WARNING: Could not restore previous backup of the games");
        } else {
            const backup = JSON.parse(data);

            for (const gameNumber in backup) {
                if (Object.prototype.hasOwnProperty.call(backup, gameNumber)) {
                    games[gameNumber] = Game.fromJSON(backup[gameNumber]);
                }
            }
        }
    } catch (e) {
        console.error("WARNING: Could not restore previous backup of the games: file is broken:");
        console.error("WARNING: ", e);
    }

    // See https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers
    server.on("upgrade", function upgradeToWebsocket(request, socket) {
        const responseAndType = [socket, REQUEST_TYPE_WEBSOCKET];

        function closeSocket() {
            responseAndType[0] = null;
        }

        socket.on("error", function (e) {
            console.error("Socket error", e.toString());
            closeSocket();
        });

        socket.on("close", closeSocket);

        const webSocketMatch = request.url.match(/\/(?::[^/]+\/)?ws\/(?<data>[\s\S]+)$/u);

        let decodedData;
        try {
            decodedData = decodeURIComponent(webSocketMatch.groups.data);
        } catch (e) {
            socket.end("HTTP/1.1 400 Bad Request");
            console.error("WS: FAILED - Could not decode the given data " + webSocketMatch.groups.data);
            return;
        }

        let message;

        if (webSocketMatch) {
            try {
                message = JSON.parse(decodedData);
            } catch (e) {
                socket.end("HTTP/1.1 400 Bad Request");
                console.error("WS: FAILED - Could not parse the given data " + decodedData);
                return;
            }
        } else {
            socket.end("HTTP/1.1 400 Bad Request");
            return;
        }

        if (request.headers.upgrade.toLowerCase() !== "websocket") {
            socket.end("HTTP/1.1 400 Bad Request");
            console.error("WS: FAILED - HTTP header 'Upgrade' is not 'websocket' " + decodedData);
            return;
        }

        socket.write(
            "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" +
            "Upgrade: WebSocket\r\n" +
            "Connection: Upgrade\r\n"
        );

        const webSocketKey = request.headers["sec-websocket-key"];

        if (webSocketKey) {
            socket.write(
                "Sec-WebSocket-Accept: " + (
                    crypto
                        .createHash("sha1")
                        .update(webSocketKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", "binary")
                        .digest("base64")
                ) + "\r\n"
            );
        }

        socket.write("\r\n");

        debuglog("WS: established " + decodedData);

        let receivedBytes = null;

        socket.on("data", function webSocketData(buffer) {
            if (receivedBytes) {
                buffer = Buffer.concat([receivedBytes, buffer]);
                receivedBytes = null;
            }

            while (true) {
                const wsMessage = webSocketGetMessage(buffer);
                if (wsMessage && wsMessage[1]) {
                    if (wsMessage[0]) {
                        debuglog("WS message: " + wsMessage[0]);
                        try {
                            message.cmds = JSON.parse(wsMessage[0]);
                        } catch (e) {
                            writeMessage(responseAndType, '{"error":1, "reason": "Unable to parse your JSON data"}');
                            return;
                        }

                        handleCommands(message, responseAndType);
                        message.cmds = null;
                    }
                } else if (wsMessage === null) {
                    if (responseAndType[0] && !socket.isDestroyed) {
                        responseAndType[0] = null;
                        socket.end(webSocketCloseBuffer);
                    }

                    return;
                }

                if (wsMessage[1] === buffer.length) {
                    break;
                }

                if (wsMessage[1] === 0) {
                    receivedBytes = buffer;
                    return;
                }

                buffer = buffer.slice(wsMessage[1]);
            }
        });

        handleCommands(message, responseAndType);
    });

    server.on("error", function (error) {
        console.error("An error happened in the HTTP server", error);
    });

    server.listen(port, function() {
        console.log("Server listening on: http://localhost:%s", port);
    });
});


process.once("SIGINT", function () {
    console.log("SIGINT received...");
    stop();
});

process.once("SIGTERM", function () {
    console.log("SIGTERM received...");
    stop();
});

process.on("uncaughtException", function (err) {
    console.log(err);
});
