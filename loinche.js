/**
 * Copyright (C) 2016-2020 Raphaël Jakse <raphael.trivabble@jakse.fr>
 * Copyright (C) 2020-2020 Amic Frouvelle <loinche-amic@sandrine.da-et-amic.fr>
 *
 * @licstart
 * This file is part of Loinche (based on Trivabble version 202005011900);.
 *
 * Loinche is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License (GNU AGPL)
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Loinche is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Loinche.  If not, see <http://www.gnu.org/licenses/>.
 * @licend
 *
 * @source: https://trivabble.1s.fr/
 * @source: https://gitlab.com/raphj/trivabble/
 */

/*global libD, myConfirm, myAlert, myPrompt*/

(function () {
    "use strict";

    const VERSION = 202005240300;

    const Conf = window.LoincheConf || {};

    function setConf(parameterName, defaultValue) {
        if (typeof Conf[parameterName] === "undefined") {
            Conf[parameterName] = defaultValue;
        }
    }

    setConf("POLLING_DELAY",        2000);
    setConf("ENABLE_WEBSOCKETS",    true);
    setConf("ENABLE_EVENT_SOURCE",  true);
    setConf("MAX_WEBSOCKET_ERRORS", 1);
    setConf("APP_PATH",             "");

    const _ = (window.libD && libD.l10n) ? libD.l10n() : function (s) {
        return s;
    };

    const loinche = window.loinche = {l10n: _};

    function format(s, v) {
        return s.replace("{0}", v);
    }

    let baize;
    let hand;
    let deck;

    const playerCards = [];

    let audioNotification;
    let audioChat;
    let chatMessages;
    let chatTextarea;
    let teamScores;
    let yourScore;
    let theirScore;
    let addYou;
    let addThem;
    let name;
    let nameOpponentLeft;
    let nameOpponentRight;
    let namePartner;
    let handOpponentLeft;
    let handOpponentRight;
    let handPartner;
    let yourTricks;
    let theirTricks;

    let blockMove = 0;
    let remainingCards = 0;
    let needsRestart = false;
    const gameInProgress = false;

    let eventSource = null;
    let webSocket = null;

    let connectionLostMessage = null;
    let boundEventShowConnectionLost = null;
    let retryPollingTimeout = 0;
    let pollingServer = false;
    let connectionStopped = false;

    let currentMessageId = 1;
    const waitingMsgs = [];

    let serverVersion = 0;

    // HTTP path url prefix for any socket (xhr,ess or ws) from browser to server access
    function getApiEntryPoint() {
        return Conf.APP_PATH + "/:loinche";
    }

    function mouseDown(ele, fun, stop) {
        const meth = stop ? "removeEventListener" : "addEventListener";
        ele[meth]("mousedown", fun, false);
    }

    function mouseUp(ele, fun, stop) {
        const meth = stop ? "removeEventListener" : "addEventListener";
        ele[meth]("mouseup", fun, false);
    }

    function mouseMove(ele, fun, stop) {
        const meth = stop ? "removeEventListener" : "addEventListener";
        ele[meth]("mousemove", fun, false);
    }

    function setHand(hand) {
        for (let i = 0; i < 8; i++) {
            setCardParent(playerCards[i], hand[i] || "");
        }
    }

    function getFreeHandSpaceIndex() {
        for (let i = 0; i < 8; i++) {
            if (!playerCards[i].getElementsByClassName("card")[0]) {
                return i;
            }
        }

        return -1;
    }

    function showConnectionLost(msg) {
        unbindEventsShowConnectionLost();

        if (!connectionLostMessage) {
            connectionLostMessage = chatMessage("", msg || _("Whoops, there is a problem. We are trying to fix it as soon as possible. Please wait a few seconds. If it persists, please contact the person who is able to fix the problem."));
            connectionLostMessage.classList.add("error");
        }
    }

    function unbindEventsShowConnectionLost() {
        if (boundEventShowConnectionLost) {
            document.body.removeEventListener("mousemove", boundEventShowConnectionLost);
            document.body.removeEventListener("mousedown", boundEventShowConnectionLost);
            document.body.removeEventListener("touchstart", boundEventShowConnectionLost);
            boundEventShowConnectionLost = null;
        }
    }

    function removeElem(elem) {
        elem.parentNode.removeChild(elem);
    }

    function connectionReady() {
        pollingReady = true;
        retriedImmediately = false;

        unbindEventsShowConnectionLost();

        if (connectionLostMessage) {
            connectionLostMessage.querySelector(".msg-content").textContent = _("The problem is solved, sorry for the inconvenience!");
            connectionLostMessage.classList.remove("error");
            connectionLostMessage.classList.add("ok");

            const removeMessage = removeElem.bind(null, connectionLostMessage);
            connectionLostMessage.addEventListener("click", removeMessage);
            connectionLostMessage.addEventListener("touch", removeMessage);

            connectionLostMessage = null;
        }
    }

    let cardInitCoords      = null;
    let cardInitMouseCoords = null;
    let cardDest            = null;
    let cardPosition        = null;
    let cardInitDest        = null;
    let cardInitPosition    = null;
    let movingCard          = null;
    let moveCMD             = null;
    let handBCR             = null;
    let baizeBCR            = null;
    let deckBCR             = null;
    let handChanged         = false;

    function getCard(l) {
        const card = l.getElementsByClassName("card")[0];
        if (!card) {
            return "";
        }

        return card.getElementsByClassName("card-name")[0].textContent;
    }

    function isParentOf(p, elem) {
        while (elem) {
            if (elem === p) {
                return true;
            }
            elem = elem.parentNode;
        }

        return false;
    }

    function dragCardEnd() {
        movingCard.style.left   = "";
        movingCard.style.top    = "";
        movingCard.style.width  = "";
        movingCard.style.height = "";

        mouseUp(document,   dragCardEnd,  true);
        mouseMove(document, dragCardMove, true);

        if (cardDest === deck) {
            moveCMD.to = "deck";
            moveCMD.indexTo = -1;
            movingCard.parentNode.removeChild(movingCard);
        } else if (cardDest) {
            if (cardDest === baize) {
		movingCard.style.top = cardPosition.top * 100 + "%";
		movingCard.style.left = cardPosition.left * 100 + "%";
		moveCMD.to = "baize";
            }
            cardDest.appendChild(movingCard);
        } else if (cardInitDest.parentNode === hand && cardInitDest.getElementsByClassName("card")[0]) {
            for (let i = 0; i < 8; i++) {
                if (!playerCards[i].getElementsByClassName("card")[0]) {
                    playerCards[i].appendChild(movingCard);
                    break;
                }
            }
        } else {
            cardInitDest.appendChild(movingCard);
            if (cardInitDest === baize) {
		movingCard.style.top = cardInitPosition.top * 100 + "%";
		movingCard.style.left = cardInitPosition.left * 100 + "%";
		moveCMD.to = "baize";
		moveCMD.positionTo = cardInitPosition;
		cardDest = baize;
            }
        }

        if (cardDest) {
            cardDest.classList.remove("card-target");

            const moveHand = {
                cmd: "setHand",
                hand: playerCards.map(getCard)
            };

            if (moveCMD.to === moveCMD.from) {
                if (moveCMD.from === "hand") {
                    sendCmds([moveHand]);
                } else if (handChanged) {
                    sendCmds([moveCMD, moveHand]);
                } else {
                    sendCmds([moveCMD]);
                }
            } else if (moveCMD.to === "hand") {
                moveHand.hand[moveCMD.indexTo] = "";
                sendCmds([moveHand, moveCMD]);
            } else {
                if (handChanged) {
                    sendCmds([moveCMD, moveHand]);
                } else {
                    sendCmds([moveCMD]);
                }
            }
        }

        cardInitMouseCoords = null;
        cardInitCoords      = null;
        cardInitDest        = null;
        cardDest            = null;
	cardPosition        = null;
        movingCard          = null;
        baizeBCR            = null;
        deckBCR             = null;
        handBCR             = null;
        moveCMD             = null;
    }

    function dragCardMove(e) {
        let newLeft = (cardInitCoords.left + (e.clientX - cardInitMouseCoords.clientX));
        let newTop  = (cardInitCoords.top  + (e.clientY - cardInitMouseCoords.clientY));


        newTop  += cardInitCoords.height / 2;
        newLeft += cardInitCoords.width / 2;
        movingCard.style.left = (newLeft + window.scrollX) + "px";
        movingCard.style.top  = (newTop  + window.scrollY) + "px";

        let newDest = null;

        if (
            (newTop  > baizeBCR.top  && newTop  < (baizeBCR.top + baizeBCR.height)) &&
            (newLeft > baizeBCR.left && newLeft < (baizeBCR.left + baizeBCR.width))
        ) {
            newDest = baize
	       
            moveCMD.to = "baize";
            cardPosition = {
		left: (newLeft - baizeBCR.left) / baizeBCR.width,
		top: (newTop - baizeBCR.top) / baizeBCR.height
	    };
	    moveCMD.positionTo = cardPosition;
        
        } else if (
            (newTop  > handBCR.top  && newTop  < (handBCR.top + handBCR.height)) &&
            (newLeft > handBCR.left && newLeft < (handBCR.left + handBCR.width))
        ) {
            const index = Math.floor(
                (
                    (newLeft - handBCR.left) / handBCR.width
                ) * playerCards.length
            );
	    const fromLeft = (((newLeft - handBCR.left) / handBCR.width) * playerCards.length - index <= 0.5)

            newDest = playerCards[index];

            if (newDest && newDest.getElementsByClassName("card")[0] && fromLeft) {
                let i = index + 1;
                let card;

                while (playerCards[i]) {
                    card = playerCards[i].getElementsByClassName("card")[0];
                    if (!card) {
                        let j = i;
                        while (j > index) {
                            handChanged = true;
                            playerCards[j].appendChild(
                                playerCards[j - 1].getElementsByClassName("card")[0]
                            );
                            j--;
                        }
                        break;
                    }
                    i++;
                }

                if (newDest.getElementsByClassName("card")[0]) {
                    i = index - 1;

                    while (playerCards[i]) {
                        card = playerCards[i].getElementsByClassName("card")[0];
                        if (!card) {
                            let j = i;
                            while (j < index) {
                                playerCards[j].appendChild(
                                    playerCards[j + 1].getElementsByClassName("card")[0]
                                );
                                j++;
                            }
                            break;
                        }
                        i--;
                    }
                }
            } else if (newDest && newDest.getElementsByClassName("card")[0]) {
                let i = index - 1;
                let card;

                while (playerCards[i]) {
                    card = playerCards[i].getElementsByClassName("card")[0];
                    if (!card) {
                        let j = i;
                        while (j < index) {
                            handChanged = true;
                            playerCards[j].appendChild(
                                playerCards[j + 1].getElementsByClassName("card")[0]
                            );
                            j++;
                        }
                        break;
                    }
                    i--;
                }

                if (newDest.getElementsByClassName("card")[0]) {
                    i = index + 1;

                    while (playerCards[i]) {
                        card = playerCards[i].getElementsByClassName("card")[0];
                        if (!card) {
                            let j = i;
                            while (j > index) {
                                playerCards[j].appendChild(
                                    playerCards[j - 1].getElementsByClassName("card")[0]
                                );
                                j--;
                            }
                            break;
                        }
                        i++;
                    }
                }
            }


            if (newDest.getElementsByClassName("card")[0]) {
                newDest = null;
            } else {
                moveCMD.to = "hand";
                moveCMD.indexTo = index;
            }
        } else if (
            (newTop  > deckBCR.top  && newTop  < (deckBCR.top + deckBCR.height)) &&
            (newLeft > deckBCR.left && newLeft < (deckBCR.left + deckBCR.width))
        ) {
            newDest = deck;
        }

        if (newDest !== cardDest) {
            if (cardDest) {
                cardDest.classList.remove("card-target");
            }

            if (newDest) {
                newDest.classList.add("card-target");
            }
        }

        cardDest = newDest;
    }

    function dragCardBegin(e) {
        preventDefault(e);

        if (blockMove || waitingMsgs.length) {
            return;
        }

        if (!pollingReady) {
            showConnectionLost();
            return;
        }

        loadAudio();

        handChanged = false;
        movingCard = e.currentTarget;

        cardInitMouseCoords = e;
        cardInitCoords      = movingCard.getBoundingClientRect();
        cardInitDest        = movingCard.parentNode;

        baizeBCR = baize.getBoundingClientRect();
        handBCR  = hand.getBoundingClientRect();
        deckBCR  = deck.getBoundingClientRect();

        let from;
        let index;

        let p = movingCard.parentNode;
        let oldP = movingCard;

        while (p) {
            if (p === baize) {
                from  = "baize";
		cardInitPosition = {
		    top: parseFloat(movingCard.style.top.slice(0,-1)) / 100,
		    left: parseFloat(movingCard.style.left.slice(0,-1)) / 100
		};
                index = Array.prototype.indexOf.call(baize.children, movingCard); 
		baize.classList.add("card-target");
                break;
            }

            if (p === hand) {
                from  = "hand";
                index = playerCards.indexOf(oldP);

                break;
            }
            oldP = p;
            p = p.parentNode;
        }

        moveCMD = {
            cmd: "moveCard",
            from: from,
            indexFrom: index
        };

        mouseMove(document, dragCardMove);
        mouseUp(document,   dragCardEnd);

	movingCard.style.transform = "translate(-50%,-50%)";
        movingCard.style.left   = cardInitCoords.left + cardInitCoords.width / 2 + window.scrollX + "px";
        movingCard.style.top    = cardInitCoords.top  + cardInitCoords.height / 2 + window.scrollY + "px";
        // movingCard.style.width  = cardInitCoords.width  + "px";
        // movingCard.style.height = cardInitCoords.height + "px";

        document.body.appendChild(movingCard);
    }

    function setCard(card, cardname) {
        card.firstChild.textContent = cardname;
        card.lastChild.src = "cards/" + cardname + ".svg";
	card.lastChild.alt = cardname;
    }

    function preventDefault(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    function makeCard(cardname, noEvents) {
        const card = document.createElement("span");
        card.translate = false;
        card.className = "card";
        card.appendChild(document.createElement("span"));
        card.lastChild.className = "card-name";
        card.appendChild(document.createElement("img"));
        card.lastChild.className = "card-image";

        if (!noEvents) {
            mouseDown(card, dragCardBegin);

            card.addEventListener("contextmenu", preventDefault);
            card.addEventListener("touchstart", preventDefault);
            card.addEventListener("touchmove", preventDefault);
        }

        setCard(card, cardname);
        return card;
    }

    function setCardParent(p, cardname) {
        let card = p.getElementsByClassName("card")[0];
        if (card) {
            if (cardname) {
                setCard(card, cardname);
            } else {
                p.removeChild(card);
            }
        } else if (cardname) {
            card = makeCard(cardname);
            p.appendChild(card);
        }
    }

    function baizePushCard(player, position, cardname, seat) {
	let card = makeCard(cardname);
	switch (((seat - parseInt(localStorage.loinchePlayerSeat)) + 4) % 4) {
		
		// Finally, we do not rotate the cards, it is better to read
	    case 0:
		card.style.top = position.top * 100 + "%";
		card.style.left = position.left * 100 + "%";
		// card.style.transform = "translate(-50%,-50%)";
		break;
	    case 1:
		card.style.top = position.left * 100 + "%";
		card.style.left = (1 - position.top) * 100 + "%";
		// card.style.transform = "translate(-50%,-50%) rotate(90deg)";
		break;
	    case 2:
		card.style.top = (1 - position.top) * 100 + "%";
		card.style.left = (1 - position.left) * 100 + "%";
		// card.style.transform = "translate(-50%,-50%) rotate(180deg)";
		break;
	    case 3:
		card.style.top = (1 - position.left) * 100 + "%";
		card.style.left = position.top * 100 + "%";
		// card.style.transform = "translate(-50%,-50%) rotate(-90deg)";
		break;
	}
	baize.appendChild(card);
    }

    function baizeRemoveCard(index) {
	let card = baize.childNodes[index];
	removeElem(card);
    }

    function setBaize(baizeData) {
	while (baize.firstChild) {
	    baize.removeChild(baize.lastChild);
	}
	for (const cardData of baizeData) {
	    baizePushCard(cardData.player, cardData.position, cardData.cardname, cardData.seat);
	}
    }

    function setTricks(cardCount, parity) {
	let theTricks = yourTricks;
	if ((parseInt(localStorage.loinchePlayerSeat) + parity) % 2 == 1) {
	    theTricks = theirTricks;
	}
	
	let oldCardCount = theTricks.children.length;
	if (oldCardCount > cardCount) {
	    let cardTrickBCR = theTricks.firstChild.getBoundingClientRect();
	    for (let i = 0; i < oldCardCount - cardCount; i++) {
		theTricks.removeChild(theTricks.lastChild);
	    }
	    theTricks.style.width = (Math.floor((cardCount - 1) / 4) * cardTrickBCR.height / 4 + ((cardCount-1) % 4) * cardTrickBCR.height / 40 + cardTrickBCR.width) + "px";
	    blink(theTricks.parentNode);
	} else if (oldCardCount < cardCount) {
	    for (let i = 0; i < cardCount - oldCardCount; i++) {
		let cardTrick = theTricks.appendChild(document.createElement("span"));
		cardTrick.translate = false;
		cardTrick.className = "card-trick";
		cardTrick.appendChild(document.createElement("img"));
		cardTrick.lastChild.className = "card-image";
		cardTrick.lastChild.src = "cards/back.svg";
		cardTrick.lastChild.alt = "back";
		let cardTrickBCR = cardTrick.getBoundingClientRect();
		cardTrick.style.top = "0px";
		cardTrick.style.left = (Math.floor((oldCardCount + i) / 4) * cardTrickBCR.height / 4 + ((oldCardCount + i) % 4) * cardTrickBCR.height / 40) + "px";
		theTricks.style.width = (Math.floor((oldCardCount + i) / 4) * cardTrickBCR.height / 4 + ((oldCardCount + i) % 4) * cardTrickBCR.height / 40 + cardTrickBCR.width) + "px";
		blink(theTricks.parentNode);
	    }
	}
    }

    function setTeamScore(score, parity) {
	let theScore = yourScore;
	if ((parseInt(localStorage.loinchePlayerSeat) + parity) % 2 == 1) {
	    theScore = theirScore;
	}
	theScore.textContent = score;
	blink(theScore);
    }
	
    function changeTeamScore(parity) {
	let theScore = yourScore;
	let whose = _("your");
	if ((parseInt(localStorage.loinchePlayerSeat) + parity) % 2 == 1) {
	    theScore = theirScore;
	    whose = _("their");
	}

	if (!pollingReady) {
            showConnectionLost();
            return;
        }
	
        myPrompt(
            format(
                _("Enter {0} new score:"),
                whose
            ),
            function (res) {
                res = parseInt(res);
                if (!isNaN(res)) {
                    sendCmds([{
                        cmd: "teamScore",
                        parity: parity,
                        score: res
                    }]);
                }
            },
            theScore.textContent
        );
    }

    function changeYourScore() {
	changeTeamScore(parseInt(localStorage.loinchePlayerSeat) % 2);
    }

    function changeTheirScore() {
	changeTeamScore((parseInt(localStorage.loinchePlayerSeat) + 1) % 2);
    }

    function addTeamScore(parity) {
	let theScore = yourScore;
	let whose = _("you");
	if ((parseInt(localStorage.loinchePlayerSeat) + parity) % 2 == 1) {
	    theScore = theirScore;
	    whose = _("them");
	}

	if (!pollingReady) {
            showConnectionLost();
            return;
        }
	
        myPrompt(
            format(
                _("Enter the score you want to add to {0}:"),
                whose
            ),
            function (res) {
                res = parseInt(res);
                if (!isNaN(res)) {
                    sendCmds([{
                        cmd: "teamScore",
                        parity: parity,
                        score: parseInt(theScore.textContent) + res
                    }]);
                }
            },
            theScore.textContent
        );
    }

    function addScoreYou() {
	addTeamScore(parseInt(localStorage.loinchePlayerSeat) % 2);
    }

    function addScoreThem() {
	addTeamScore((parseInt(localStorage.loinchePlayerSeat) + 1) % 2);
    }
    
    function set(key, value) {
        switch (key) {
            case "playerName":
                name.textContent = localStorage.loinchePlayerName = value;
                break;
            case "playerSeat":
	        // console.log("Setting seat : " + value);
                localStorage.loinchePlayerSeat = parseInt(value);
                break;
            case "gameNumber":
                document.getElementById("number").textContent = localStorage.loincheGameNumber = value;
                break;
        }
    }

    function checkGameInProgress(f) {
        if (!gameInProgress) {
            return f();
        }

        myConfirm(
            _("Your game is not over. Are you sure you want to leave now?"),
            function () {
                myAlert(
                    format(
                        _("You are about to leave the current game. To recover it, please note its number: {0}"),
                        localStorage.loincheGameNumber
                    ),
                    f
                );
            }
        );
    }

    let pollingReady = false;
    let retriedImmediately = false;

    function forceReload(msg) {
        needsRestart = true;
        myConfirm(
            msg,
            function () {
                location.reload();
            }, function () {
                blockMove = 1000;
                document.getElementById("panel").appendChild(document.createElement("a"));
                document.getElementById("panel").lastChild.href = "#";
                document.getElementById("panel").lastChild.onclick = location.reload.bind(location);
                document.getElementById("panel").lastChild.textContent = _("To continue playing, click here");
            }
        );
    }

    function fatalError(e) {
        if (!needsRestart) {
            // No need to show the user that a problem just happened if a message asking to restart has already been shown.
            forceReload(_("Sorry, a problem just happened. The page must be reloaded. If the problem is not too serious, you should be able to keep playing normally. Otherwise, contact the person who is able to fix the problem. Click on “Yes” to reload the page."));
        }

        throw (e || new Error("interrupting the game because of a fatal error"));
    }

    function jsonError(json, e) {
        console.error("An error ocurred while parsing this JSON message:", json);
        fatalError(e);
    }

    function setHandCell(index, cardname) {
        setCardParent(playerCards[index], cardname);
    }

    function blinkTransitionEnd(element) {
        element.removeEventListener("animationend", element._te, false);
        element.classList.remove("blink");
    }

    function blink(element) {
        element._te = blinkTransitionEnd.bind(null, element);
        element.addEventListener("animationend", element._te, false);
        setTimeout(element._te, 5000);
        element.classList.add("blink");
    }

    function chatMessage(sender, content) {
        const msgDom = document.createElement("div");

        msgDom.className = "msg";

        if (sender) {
            msgDom.appendChild(document.createElement("span"));
            msgDom.lastChild.className = "msg-sender";
            msgDom.lastChild.textContent = _("{0}: ").replace("{0}", sender);
        }

        if (content instanceof Element) {
            msgDom.appendChild(content);
        } else {
            msgDom.appendChild(document.createElement("span"));
            msgDom.lastChild.className = "msg-content";
            msgDom.lastChild.textContent = content;
        }

        chatMessages.appendChild(msgDom);

        chatMessages.scrollTop = chatMessages.scrollHeight;

        if (sender && sender !== localStorage.loinchePlayerName) {
            if (msgSound.checked) {
                audioChat.play();
            }

            blink(chatMessages);
        }

        return msgDom;
    }

    function handleChatMessage(msg) {
        if (msg.specialMsg) {
	    switch (msg.specialMsg.type) {
		case "cut": {
		    const content = document.createElement("div");
		    content.appendChild(document.createElement("span"));
		    content.lastChild.className = "info";
		    content.lastChild.textContent = format(_("{0} has cut the deck."), msg.sender);
		    chatMessage("", content);
		    return;
		}
            }
	}

        chatMessage(msg.sender, msg.content);
    }

    function handleReceivedData(data) {
        if (Array.isArray(data)) {
            data.forEach(handleReceivedData);
            return;
        }

        if (data.id) {
            const pos = waitingMsgs.indexOf(data.id);
            if (pos !== -1) {
                waitingMsgs.splice(pos, 1);
            }
        }

        if (data.version) {
            serverVersion = data.version;
        }

        if (data.pleaseRestart) {
            forceReload(_("Sorry to disturb you, but we need to reload the game so you can continue playing. You will not lose anything except the messages in the chat. Are you ready?"));
        }

        if (data.error) {
            fatalError(new Error("Error from the server: " + data.error + " - " + data.reason));
        }

        if (data.stopping) {
            stopConnection();

            retryPolling(
                (typeof data.stopping === "number" && data.stopping > 0)
                    ? data.stopping
                    : 0
            );
        }

        if (data.msg) {
            handleChatMessage(data.msg);
        }

	if (typeof data.teamScore === "number" || typeof data.teamScore === "string") {
	    setTeamScore(data.teamScore, data.parity)
	}

        if (data.players) {
	    
            for (let i = 0; i < data.players.length; i++) { // first check if seat has been changed
                const player = data.players[i];
                const playerName = data.players[i].player;
                const playerSeat = data.players[i].seat;
		if (playerName == localStorage.loinchePlayerName) {
		    set("playerSeat", playerSeat);
		}
	    }
		
            for (let i = 0; i < data.players.length; i++) {
                const player = data.players[i];
                const playerName = data.players[i].player;
                const playerSeat = data.players[i].seat;

                if (Object.prototype.hasOwnProperty.call(player, "handCount")) {
		    if (playerSeat >= 0) {
			switch ((playerSeat - parseInt(localStorage.loinchePlayerSeat) + 4) % 4) {  // buggy javascript mod ;-)
			    case 1: {
			        nameOpponentLeft.textContent = playerName; 
			        blink(nameOpponentLeft);
				let oldHandCount = handOpponentLeft.children.length;
				if (oldHandCount > player.handCount) {
				    let cardOpponentLeftBCR = handOpponentLeft.firstChild.getBoundingClientRect();
				    for (let k = 0; k < oldHandCount - player.handCount; k++) {
					handOpponentLeft.removeChild(handOpponentLeft.lastChild);
				    }
				    handOpponentLeft.style.height = ((player.handCount - 1) * cardOpponentLeftBCR.width / 4 + cardOpponentLeftBCR.height) + "px";
				    blink(handOpponentLeft.parentNode);
				} else if (oldHandCount < player.handCount) {
				    for (let k = 0; k < player.handCount - oldHandCount; k++) {
					let cardOpponentLeft = handOpponentLeft.appendChild(document.createElement("span"));
					cardOpponentLeft.translate = false;
					cardOpponentLeft.className = "card-opponent";
					cardOpponentLeft.appendChild(document.createElement("img"));
					cardOpponentLeft.lastChild.className = "card-image";
					cardOpponentLeft.lastChild.src = "cards/back.svg";
					cardOpponentLeft.lastChild.alt = "back";
					let cardOpponentLeftBCR = cardOpponentLeft.getBoundingClientRect();
					cardOpponentLeft.style.left = "0px";
					cardOpponentLeft.style.top = ((oldHandCount + k) * cardOpponentLeftBCR.width / 4) + "px";
					handOpponentLeft.style.height = ((oldHandCount + k) * cardOpponentLeftBCR.width / 4 + cardOpponentLeftBCR.height) + "px";
				    }
				}
			        break;
			    }
			    case 3: {
			        nameOpponentRight.textContent = playerName; // + " - " + player.handCount;
			        blink(nameOpponentRight);
				let oldHandCount = handOpponentRight.children.length;
				if (oldHandCount > player.handCount) {
				    let cardOpponentRightBCR = handOpponentRight.firstChild.getBoundingClientRect();
				    for (let k = 0; k < oldHandCount - player.handCount; k++) {
					handOpponentRight.removeChild(handOpponentRight.firstChild);
				    }
				    handOpponentRight.style.height = ((player.handCount - 1) * cardOpponentRightBCR.width / 4 + cardOpponentRightBCR.height) + "px";
				    blink(handOpponentRight.parentNode);
				} else if (oldHandCount < player.handCount) {
				    for (let k = 0; k < player.handCount - oldHandCount; k++) {
					let cardOpponentRight = handOpponentRight.insertBefore(document.createElement("span"),handOpponentRight.firstChild);
					cardOpponentRight.translate = false;
					cardOpponentRight.className = "card-opponent";
					cardOpponentRight.appendChild(document.createElement("img"));
					cardOpponentRight.lastChild.className = "card-image";
					cardOpponentRight.lastChild.src = "cards/back.svg";
					cardOpponentRight.lastChild.alt = "back";
					let cardOpponentRightBCR = cardOpponentRight.getBoundingClientRect();
					cardOpponentRight.style.left = "0px";
					cardOpponentRight.style.top = ((oldHandCount + k) * cardOpponentRightBCR.width / 4) + "px";
					handOpponentRight.style.height = ((oldHandCount + k) * cardOpponentRightBCR.width / 4 + cardOpponentRightBCR.height) + "px";
				    }
				}
			        break;
			    }
			    case 2: {
			        namePartner.textContent = playerName; // + " - " + player.handCount;
			        blink(namePartner);
				let oldHandCount = handPartner.children.length;
				if (oldHandCount > player.handCount) {
				    let cardPartnerBCR = handPartner.firstChild.getBoundingClientRect();
				    for (let k = 0; k < oldHandCount - player.handCount; k++) {
					handPartner.removeChild(handPartner.firstChild);
				    }
				    handPartner.style.width = ((player.handCount - 1) * cardPartnerBCR.height / 4 + cardPartnerBCR.width) + "px";		    
				    blink(handOpponentRight.parentNode);
				} else if (oldHandCount < player.handCount) {
				    for (let k = 0; k < player.handCount - oldHandCount; k++) {
					let cardPartner = handPartner.insertBefore(document.createElement("span"),handPartner.firstChild);
					cardPartner.translate = false;
					cardPartner.className = "card-partner";
					cardPartner.appendChild(document.createElement("img"));
					cardPartner.lastChild.className = "card-image";
					cardPartner.lastChild.src = "cards/back.svg";
					cardPartner.lastChild.alt = "back";
					let cardPartnerBCR = cardPartner.getBoundingClientRect();
				cardPartner.style.top = "0px";
					cardPartner.style.left = ((oldHandCount + k) * cardPartnerBCR.height / 4) + "px";
					handPartner.style.width = ((oldHandCount + k) * cardPartnerBCR.height / 4 + cardPartnerBCR.width) + "px";
				    }
				}
			        break;
			    }
			}	
		    }
                }
            }
        }

        if (data.playerName) {
            set("playerName", data.playerName);
        }

        if (typeof data.playerSeat === "number") {
            set("playerSeat", data.playerSeat);
        }

        if (data.gameNumber) {
            set("gameNumber", data.gameNumber);
        }

        if (data.baize) {
            setBaize(data.baize);
        }

	if (typeof data.evenTricksLength === "number") {
	    setTricks(data.evenTricksLength,0);
	}
	if (typeof data.oddTricksLength === "number") {
	    setTricks(data.oddTricksLength,1);
	}
		
	if (typeof data.remainingCards === "number") {
            remainingCards = data.remainingCards;
            document.getElementById("remaining-cards").textContent = " " + data.remainingCards;
        }
        
        if (data.hand) {
	    if (!data.player || data.player == localStorage.loinchePlayerName) {
		setHand(data.hand);
	    }
	}

        if (!data.action) {
            return;
        }

        switch (data.action) {
            case "putDeck": //TODO Not needed ? Nothing to do ?
                break;

            case "takeDeck": //TODO Not needed ? Nothing to do ?
                break;

            case "reset": //TODO : clear seats and other hands.
		namePartner.textContent = nameOpponentRight.textContent = nameOpponentLeft.textContent = _("(pending)");
		while (handOpponentLeft.firstChild) { handOpponentLeft.removeChild(handOpponentLeft.lastChild);}
		while (handPartner.firstChild) { handPartner.removeChild(handPartner.lastChild);}
		while (handOpponentRight.firstChild) { handOpponentRight.removeChild(handOpponentRight.lastChild);}
                sendCmds([{cmd: "hello", wantedSeat: parseInt(localStorage.loinchePlayerSeat)}]);
                break;

	    case "showTrick": //TODO ? Not needed : it gives a baize which is set again (could give less loading though, but it seems to be ok).
	        break;
	    
	    case "takeTrick": //TODO ? Same thing. Now the baize is just cleared.
	        break;
	    
	    case "moveCard":
                if (data.from === "baize") {
		    // Only do it when asked from another player : if the player itself is doing it, it is already removed.
		    if (data.player !== localStorage.loinchePlayerName) {
			baizeRemoveCard(data.index);
		    }
                } else if (data.from === "hand") {
                    setHandCell(data.indexFrom, "");
                }

                if (data.to === "baize") {
		    // Only do it when asked from another player : if the player itself is doing it, it is already there.
		    //if (data.player !== localStorage.loinchePlayerName) {
			baizePushCard(data.player, data.position, data.cardname, data.seat);
		    //}
                } else if (data.to === "hand") {
                    setHandCell(data.indexTo, data.cardname);
                }
                break;
	     	    
	    case "baizeRemoveCard":
                // Only do it when asked from another player : if the player itself is doing it, it is already removed.
	        if (data.player !== localStorage.loinchePlayerName) {
		    baizeRemoveCard(data.index);
		}
	        break;

	    case "baizePushCard":
                // Only do it when asked from another player : if the player itself is doing it, it is already there.
		if (data.player !== localStorage.loinchePlayerName) {
		    if (cardsSound.checked) {
			audioNotification.play();
		    }
		    baizePushCard(data.player, data.position, data.cardname, data.seat);
		}
                break;
	    
	    case "setHandCell":
                setHandCell(data.indexTo, data.cardname);

        }
    }

    function retryPolling(delay) {
        if (needsRestart) {
            return;
        }

        pollingReady = false;

        if (!boundEventShowConnectionLost) {
            boundEventShowConnectionLost = showConnectionLost.bind(null, null);

            document.body.addEventListener("mousemove", boundEventShowConnectionLost);
            document.body.addEventListener("mousedown", boundEventShowConnectionLost);
            document.body.addEventListener("touchstart", boundEventShowConnectionLost);
        }

        if (retryPollingTimeout) {
            return;
        }

        if (delay || retriedImmediately) {
            retryPollingTimeout = setTimeout(
                function () {
                    connectionStopped = false;
                    pollingServer = false;
                    startConnection();
                    retryPollingTimeout = 0;
                },
                delay || Conf.POLLING_DELAY
            );
        } else {
            retriedImmediately = true;
            connectionStopped = false;
            pollingServer = false;
            startConnection();
        }
    }

    function parseAndHandleReceivedData(r) {
        let m;

        try {
            m = JSON.parse(r);
        } catch (e) {
            jsonError(r, e);
        }

        try {
            handleReceivedData(m);
        } catch (e) {
            fatalError(e);
        }
    }

    function closeConnections() {
        if (eventSource) {
            eventSource.onerror = null;
            eventSource.close();
            eventSource = null;
        }

        if (webSocket) {
            webSocket.onclose = null;
            webSocket.onerror = null;
            webSocket.close();
            webSocket = null;
        }
    }

    function stopConnection() {
        closeConnections();

        if (retryPollingTimeout) {
            clearTimeout(retryPollingTimeout);
            retryPollingTimeout = 0;
        }

        connectionStopped = true;
        pollingServer = false;
        pollingReady = false;
    }

    let blacklistWebsockets = false;
    let webSocketErrors = 0;

    function bindConnectionEvents(connection) {
        connection.onopen = connectionReady;

        connection.onmessage = function (e) {
            if (connection === webSocket) {
                webSocketErrors = 0;
            }

            connectionReady();
            parseAndHandleReceivedData(e.data);
        };

        connection.onerror = function (e) {
            console.error("Connection error:", e);

            if (connection === eventSource) {
                retryPolling();
            } else {
                webSocketErrors++;

                if (webSocketErrors > Conf.MAX_WEBSOCKET_ERRORS) {
                    blacklistWebsockets = true;
                }
            }
        };

        if (connection === webSocket) {
            connection.onclose = function (e) {
                webSocket = null;
                if (e.code === 1002 || e.code === 1003) {
                    // CLOSE_PROTOCOL_ERROR or CLOSE_UNSUPPORTED
                    blacklistWebsockets = true;
                    pollServerWithEventSource();
                    return;
                }
                retryPolling();
            };
        }
    }

    function canConnect() {
        return !pollingServer && !needsRestart && !connectionStopped && navigator.onLine !== false;
        // DO NOT change navigator.onLine !== false by navigator.onLine === true
        // This would block connection on a browser not supporting this property
    }

    function pollServerWithEventSource() {
        if (canConnect() && Conf.ENABLE_EVENT_SOURCE && window.EventSource) {
            closeConnections();
            pollingServer = true;

            eventSource = new EventSource(getApiEntryPoint() + "/sse/" + JSON.stringify(cmdsWithContext()));
            bindConnectionEvents(eventSource);
            return;
        }

        pollServerWithXHR();
    }

    function pollServerWithWebSocket() {
        if (canConnect() && Conf.ENABLE_WEBSOCKETS && !blacklistWebsockets && window.WebSocket) {
            closeConnections();
            pollingServer = true;

            webSocket = new WebSocket(
                (window.location.protocol === "http:" ? "ws://" : "wss://") +
                window.location.host +
                getApiEntryPoint() + "/ws/" +
                JSON.stringify(cmdsWithContext())
            );

            bindConnectionEvents(webSocket);
            return;
        }

        pollServerWithEventSource();
    }

    function xhrRequest(data, onreadystatechange) {
        const xhr = new XMLHttpRequest();

        xhr.open("POST", getApiEntryPoint(), true);
        xhr.setRequestHeader("Content-Type", "text/plain");
        xhr.send(JSON.stringify(data));

        xhr.onreadystatechange = onreadystatechange.bind(null, xhr);
    }

    function pollServerWithXHR() {
        if (!canConnect()) {
            return;
        }

        pollingServer = true;

        let currentIndex = 0;
        let expectedLength = 0;

        xhrRequest(cmdsWithContext(), function (xhr) {
            if (xhr.readyState === 2) {
                connectionReady();
            } else if (xhr.readyState === 3) {
                connectionReady();

                while (true) {
                    if (!expectedLength) {
                        let i = currentIndex;
                        while (i < xhr.responseText.length) {
                            if ("0123456789".indexOf(xhr.responseText.charAt(i)) === -1) {
                                expectedLength = parseInt(xhr.responseText.substring(currentIndex, i));
                                currentIndex = i;
                                break;
                            }
                            ++i;
                        }
                    }

                    if (expectedLength && (xhr.responseText.length >= currentIndex + expectedLength)) {
                        const end = currentIndex + expectedLength;
                        let msgs;

                        try {
                            msgs = JSON.parse(
                                xhr.responseText.substring(
                                    currentIndex,
                                    end
                                )
                            );
                            currentIndex = end;
                            expectedLength = 0;
                        } catch (e) {
                            jsonError(xhr.responseText.substring(
                                currentIndex,
                                end
                            ), e);
                        }

                        handleReceivedData(msgs);
                    } else {
                        break;
                    }
                }
            } else if (xhr.readyState === 4) {
                startConnection();
            }
        });
    }

    function startConnection() {
        pollServerWithWebSocket();
    }

    function sendXHR(cmds) {
        if (needsRestart) {
            return;
        }

        blockMove++;

        xhrRequest(cmdsWithContext(cmds), function (xhr) {
            if (xhr.readyState === 4) {
                if (xhr.status === 0 || xhr.status >= 300) {
                    setTimeout(sendCmds.bind(null, cmds), Conf.POLLING_DELAY);
                    return;
                }

                parseAndHandleReceivedData(xhr.responseText);

                blockMove--;

                if (!pollingReady) {
                    startConnection();
                }
            }
        });
    }

    function cmdsWithContext(cmds) {
        return {
            gameNumber: localStorage.loincheGameNumber || "",
            playerName: localStorage.loinchePlayerName,
            version: VERSION,
            cmds: cmds
        };
    }

    function sendCmds(cmds) {
        if (webSocket && webSocket.readyState === 1) {
            if (serverVersion >= 202004281800) {
                for (const cmd of cmds) {
                    waitingMsgs.push(currentMessageId);
                    cmd.id = currentMessageId++;
                }
            }

            webSocket.send(JSON.stringify(cmds));
            return;
        }

        if (!pollingReady) {
            startConnection();
            setTimeout(sendCmds, Conf.POLLING_DELAY, cmds);
            return;
        }

        sendXHR(cmds);
    }

    function joinGame() {
        checkGameInProgress(
            function () {
                myPrompt(
                    _("To join a game, please give the number which is displayed on other players' screen.\nIf you do not know it, ask them.\n\nWarning: they must not take your number, they keep their own. If you wish to recover your current game, please note the following number: {0}.").replace("{0}", localStorage.loincheGameNumber),
                    function (n) {
                        n = parseInt(n);
                        if (isNaN(n)) {
                            myAlert(_("It seems your did not give a correct number, or you clicked on “Cancel”. As a result, the current game continues, if any. To join a game, click on “Join a game” again."));
                        } else {
                            localStorage.loincheGameNumber = n;
                            location.reload();
                        }
                    }
                );
            }
        );
    }

    function changeName() {
        myPrompt(
            _("To change your name, enter a new one. You can keep using your current name by cancelling. Please note that if you change your name and you have games in progress, you will not be able to keep playing them anymore unless you get back to your current name."),
            function (newName) {
                if (newName && newName.trim()) {
                    localStorage.loinchePlayerName = newName.trim();
                    location.reload();
                }
            },
            localStorage.loinchePlayerName
        );
    }

    function exchangeLeft() {
	const theSeat = parseInt(localStorage.loinchePlayerSeat);
	sendCmds(
            [{cmd: "exchangeSeats", from: theSeat, to: (theSeat + 1) % 4}]
        );
    }

    function exchangePartner() {
	const theSeat = parseInt(localStorage.loinchePlayerSeat);
	sendCmds(
            [{cmd: "exchangeSeats", from: theSeat, to: (theSeat + 2) % 4}]
        );
    }

    function exchangeRight() {
	const theSeat = parseInt(localStorage.loinchePlayerSeat);
	sendCmds(
            [{cmd: "exchangeSeats", from: theSeat, to: (theSeat + 3) % 4}]
        );
    }
    
    function startGame(number) {
        if (number) {
            localStorage.loincheGameNumber = number;
        }
        startConnection();
    }

    let audioCardLoaded = false;
    let audioMsgLoaded = false;
    let cardsSound;
    let msgSound;

    function loadAudio() {
        if (!audioCardLoaded && cardsSound.checked) {
            audioCardLoaded = true;
            audioNotification.load();
        }

        if (!audioMsgLoaded  && msgSound.checked) {
            audioMsgLoaded = true;
            audioChat.load();
        }
    }

 
    function deckClicked(e) {
        preventDefault(e);

        if (blockMove || waitingMsgs.length) {
            return;
        }

        if (!remainingCards) {
            myAlert(_("You cannot take another card: the deck is empty."));
            return;
        }

        loadAudio();

        if (!pollingReady) {
            showConnectionLost();
            return;
        }

        const index = getFreeHandSpaceIndex();
        if (index === -1) {
            myAlert(_("You cannot take another card: your hand is full."));
        } else {
            sendCmds(
                [{cmd: "moveCard", from: "deck", to: "hand", indexTo: index}]
            );
        }
    }

    function clearGame() {
        myConfirm(
            _("Are you sure you want to reset seats, scores, and take all the cards, make a new deck and shuffle it (in order to play another game)?"),
            function () {
                sendCmds([{cmd: "resetGame"}]);
            }
        );
    }

    function clearHand() {
        myConfirm(
            _("Are you sure you want to put all your cards back on the baize?"),
            function () {
                sendCmds([ {cmd: "clearHand"} ]);
            }
        );
    }

    /* function showRack() {
        myConfirm(
            _("Are you sure you want to show your rack to everybody?"),
            function () {
                const letters = playerLetters.map(
                    function (tilePlaceholer) {
                        return tilePlaceholer.querySelector(".tile");
                    }
                ).filter(function (tile) {
                    return tile !== null;
                });

                sendCmds([{
                    cmd: "msg",
                    msg: _("Here is my rack:") + " " + letters.map(
                        function (tile) {
                            return (
                                tile.textContent === ""
                                    ? _("joker")
                                    : tile.querySelector(".tile-letter").textContent
                            );
                        }
                    ).join(", "),
                    specialMsg: {
                        type: "rack",
                        rack: letters.map(
                            function (tile) {
                                return tile.querySelector(".tile-letter").textContent;
                            }
                        )
                    }
                }]);
            }
        );

    }

    function showHand() {
    } */

    function deal2() {
	deal(2);
    }

    function deal3() {
	deal(3);
    }

    function deal(nCards) {
	let theSeat = parseInt(localStorage.loinchePlayerSeat)
	sendCmds([
	    {cmd: "deal", seatTo: (theSeat + 1) % 4, nCards: nCards},
	    {cmd: "deal", seatTo: (theSeat + 2) % 4, nCards: nCards},
	    {cmd: "deal", seatTo: (theSeat + 3) % 4, nCards: nCards},
	    {cmd: "deal", seatTo: theSeat, nCards: nCards}
	]);
    }
    
    function seeYourLast() {
	sendCmds([{cmd: "showTrick", parity: (localStorage.loinchePlayerSeat) % 2}]);
    }

    function seeTheirLast() {
	sendCmds([{cmd: "showTrick", parity: (1 - (localStorage.loinchePlayerSeat) % 2)}]);
    }

    function makeDeck() {
	sendCmds([{cmd: "makeDeck", parityAbove: (localStorage.loinchePlayerSeat) % 2}]);
    }
    
    function cutDeck() {
	myPrompt(
            _("Where to cut? Give a number between 1 and {0} (the number of cards in the lower part of the deck when cutting).").replace("{0}", remainingCards - 1),
            function (n) {
                n = parseInt(n);
                if (isNaN(n) || n <= 0 || n >= remainingCards ) {
                    myAlert(_("It seems your did not give a correct number, or you clicked on “Cancel”. As a result, the current game continues, if any. To cut the deck, click on “Cut the deck” again."));
                } else {
		    sendCmds([
			{cmd: "cutDeck", indexCut: n},
			{cmd: "msg", specialMsg: {type: "cut"}}
		    ]);
                }
            }
        );
	
    }

    function takeTrick() {
	sendCmds([{cmd: "takeTrick", parity: (localStorage.loinchePlayerSeat) % 2}]);
    }

    function giveTrick() {
	sendCmds([{cmd: "takeTrick", parity: (1 - (localStorage.loinchePlayerSeat) % 2)}]);
    }
    
    function initChat() {
        chatMessages.style.width = chatMessages.offsetWidth + "px";

        const btn = document.getElementById("chat-btn");

        chatTextarea.onmouseup = function () {
            chatMessages.style.width = chatTextarea.offsetWidth + "px";
        };

        btn.onclick = function () {
            if (!pollingReady) {
                return;
            }

            loadAudio();
            sendCmds([{
                cmd: "msg",
                msg: chatTextarea.value
            }]);

            chatTextarea.value = "";
        };

        chatTextarea.onkeydown = function (e) {
            if (e.keyCode === 13) {
                preventDefault(e);
                chatTextarea.focus();
                btn.onclick();
            }
        };
    }

    function initSound() {
        audioNotification = new Audio();
        audioNotification.preload = "auto";
        audioNotification.volume  = 1;

        let audioSourceOGG = document.createElement("source");
        audioSourceOGG.src = "notification.ogg";

        let audioSourceMP3 = document.createElement("source");
        audioSourceMP3.src = "notification.mp3";
        audioNotification.appendChild(audioSourceOGG);
        audioNotification.appendChild(audioSourceMP3);

        audioChat = new Audio();
        audioChat.preload = "auto";
        audioChat.volume  = 1;

        audioSourceOGG = document.createElement("source");
        audioSourceOGG.src = "receive.ogg";

        audioSourceMP3 = document.createElement("source");
        audioSourceMP3.src = "receive.mp3";
        audioChat.appendChild(audioSourceOGG);
        audioChat.appendChild(audioSourceMP3);


        cardsSound = document.getElementById("cards-sound");
        msgSound = document.getElementById("msg-sound");

        cardsSound.onclick = function () {
            localStorage.loincheCardSound = cardsSound.checked;
        };

        msgSound.onclick = function () {
            localStorage.loincheMsgSound = msgSound.checked;
        };

        if (Object.prototype.hasOwnProperty.call(localStorage, "loincheMsgSound")) {
            msgSound.checked = localStorage.loincheMsgSound === "true";
        } else {
            localStorage.loincheMsgSound = msgSound.checked;
        }

        if (Object.prototype.hasOwnProperty.call(localStorage, "loincheCardSound")) {
            cardsSound.checked = localStorage.loincheCardSound === "true";
        } else {
            localStorage.loincheCardsSound = cardsSound.checked;
        }
    }

    function repromptName(f) {
        if (localStorage.loinchePlayerName && localStorage.loinchePlayerName.trim()) {
            f();
        } else {
            myPrompt(
                _("It seems your did not give your name. You need to do it for the game to run properly."),
                function (name) {
                    if (name && name.trim()) {
                        localStorage.loinchePlayerName = name.trim();
                    }

                    repromptName(f);
                }
            );
        }
    }

    loinche.applyL10n = function () {
        document.documentElement.lang = libD.lang;
        document.documentElement.setAttribute("xml:lang", libD.lang);

        for (const node of [].slice.call(document.querySelectorAll("[data-l10n]"))) {
            if (node.dataset.l10n === "text-content") {
                node.textContent = _(node.textContent.trim());
            } else {
                node.setAttribute(node.dataset.l10n, _(node.getAttribute(node.dataset.l10n)));
            }
        }

        loinche.run();
    };

    function langSelectionChange(e) {
        localStorage.loincheLang = e.target.value;
        location.reload();
    }

    function initGlobals() {
        baize             = document.getElementById("baize");
        hand              = document.getElementById("hand");
        handOpponentLeft  = document.getElementById("hand-left");
        handOpponentRight = document.getElementById("hand-right");
        handPartner       = document.getElementById("hand-partner");
        yourTricks        = document.getElementById("your-tricks");
        theirTricks       = document.getElementById("opponents-tricks");
	teamScores        = document.getElementById("team-scores");
	yourScore         = document.getElementById("your-score");
	theirScore        = document.getElementById("their-score");
	addYou            = document.getElementById("add-you");
	addThem           = document.getElementById("add-them");
        name              = document.getElementById("name");
        nameOpponentLeft  = document.getElementById("name-opponent-left");
        nameOpponentRight = document.getElementById("name-opponent-right");
        namePartner       = document.getElementById("name-partner");
        deck              = document.getElementById("deck");
        chatMessages      = document.getElementById("chat-messages");
        chatTextarea      = document.getElementById("chat-ta");
    }

    function initEvents() {
        mouseDown(deck, deckClicked);

        document.getElementById("clear-game").onclick       = clearGame;
        document.getElementById("deal-2").onclick           = deal2;
        document.getElementById("deal-3").onclick           = deal3;
        document.getElementById("see-your-last").onclick    = seeYourLast;
        document.getElementById("see-their-last").onclick   = seeTheirLast;
        document.getElementById("make-deck").onclick        = makeDeck;
        document.getElementById("cut-deck").onclick         = cutDeck;
        document.getElementById("take-trick").onclick       = takeTrick;
        document.getElementById("give-trick").onclick       = giveTrick;
        document.getElementById("change-name").onclick      = changeName;
        document.getElementById("exchange-left").onclick    = exchangeLeft;
        document.getElementById("exchange-partner").onclick = exchangePartner;
        document.getElementById("exchange-right").onclick   = exchangeRight;
        document.getElementById("join-game").onclick        = joinGame;
        document.getElementById("clear-hand").onclick       = clearHand;
        // document.getElementById("show-hand").onclick        = showHand;

	addYou.onclick     = addScoreYou;
	addThem.onclick    = addScoreThem;
	yourScore.onclick  = changeYourScore;
	theirScore.onclick = changeTheirScore;
	
    }

    function initGame() {
        if (!localStorage.loinchePlayerName) {
            myPrompt(
                _("Hello! To begin, enter your name. Your adversaries will see this name when you play with them."),
                function (name) {
                    if (name && name.trim()) {
                        localStorage.loinchePlayerName = name;
                    }
                    repromptName(initGame);
                }
            );

            return;
        }

        name.textContent = localStorage.loinchePlayerName;

        for (let i = 0; i < 8; i++) {
            const span = document.createElement("span");
            span.className = "card-placeholder";
            hand.appendChild(span);
            playerCards.push(span);
        }


        if (localStorage.loincheGameNumber) {
            document.getElementById("number").textContent = localStorage.loincheGameNumber;
        }

        startGame(localStorage.loincheGameNumber);
    }

    function initLang() {
        const lang = libD.lang = localStorage.loincheLang || libD.lang;

        const langSel = document.getElementById("select-lang");
        langSel.value = lang;
        langSel.onchange = langSelectionChange;

        const script = document.createElement("script");
        script.src = "l10n/js/" + lang + ".js";
        script.onerror = loinche.l10nError;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    loinche.run = function () {
        initGlobals();
        initEvents();
        initChat();
        initGame();
        initSound();
    };

    loinche.l10nError = loinche.run;

    window.addEventListener("DOMContentLoaded", initLang);

    window.addEventListener("beforeunload", stopConnection);

    window.addEventListener("offline", function () {
        stopConnection();
        showConnectionLost(_("It seems your browser went offline. Please reconnect to continue playing."));
    });

    window.addEventListener("online", startConnection);

}());
